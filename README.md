SourceDog
=========
Monitors your source tree. Makes useful information available to you. Keep this tool running while
you develop!

Global Installation
-------------------

    # Install globally
    composer global require ennerd/sourcedog

    # The file is located in ~/.config/composer/vendor/bin/sourcedog
    # Add this to your path by appending the following to your ~/.profile or ~/.bashrc file
    PATH=$PATH:~/.config/composer/vendor/bin


Per Project Installation
------------------------

    # Require sourcedog in your project
    composer require --dev ennerd/sourcedog

    # The file will be placed in your vendor/bin directory
    # You should add this path to your PATH environment variable by appending this to your
    # ~/.profile or ~/.bashrc file. Replace ~/vendor/ with your actual vendor path.
    PATH=$PATH:~/vendor/bin



Basic usage example
-------------------

Scan all files in the current folder

    # sourcedog


Monitor all files using inotify to watch for modifications.

    # sourcedog -m

Other options:

    # sourcedog --help


Options file
------------

You can create an options file at `~/sourcedog.yaml`. Currently only one option is supported:

    tail:
      - /var/log/nginx/*.log
      - /var/log/mysql/*.log
      - /var/log/mysql*.log
      - /var/log/php*.log

This will cause SourceDog to search for files matching the patterns then run `tail -f -n 0 <your files>` in the
background to output any new lines.

Linting
-------
SourceDog will lint all modified files of a supported type. Currently supports .css, .js, .json, .yaml and .php.


Contributing
------------

If you wish to add lint support for other file types, create a composer package that depends on ennerd/sourcedog
and register your linter by doing the following in a composer.json autoloaded file.

    $GLOBALS['SourceDogLinters'][] = MyClassName::class;

Your class must extend `SourceDog\Linters\Linter`.

I will also accept pull requests.


Ideas
-----

* Database for recording metadata and mistake prevention?
    * Changed files. Time spent. (Understands nano, vim, vi temp files?)
    * Backup of all files? Shorttime backup when detecting .filename.ext.swp files?
* Special support for common open source software like WordPress. Vulnerability database?
* Heuristic scan for backdoors and known vulnerabilities.
* Post process various file types automatically:
    * yourfile.ts -> yourfile.js (using tsc)
    * yourfile.js -> yourfile.min.js (minify)
    * yourfile.scss -> yourfile.css
    * yourfile.css -> yourfile.min.css
    * yourfile.js -> yourfile.js.map
    * yourfile.less -> yourfile.css
* Watch error and access logs.
* Output git diff and/or git status after files have changed.
* Feature for requiring special ubuntu packages like php-mbstring etc? To help installing on fresh servers.
