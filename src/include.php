<?php
declare(strict_types=1);
namespace SourceDog;

/**
 * Not using the composer autoloader, because this tool needs to be able to work outside of composer environments
 */
spl_autoload_register(function ($className) {
    if (substr($className, 0, 10)==='SourceDog\\') {
        if(file_exists($path = __DIR__.str_replace('\\', DIRECTORY_SEPARATOR, substr($className, 9)).'.php')) {
            require($path);
        } else {
            throw new Exception("Class $className not found");
        }
    }
});

/**
 * Composer packages may extend SourceDog by registering their modules in this array during composer autoload phase.
 *
 * NOTE! There is no reason to do:
 *
 *     if(!isset($GLOBALS['SourceDog'])) {
 *         $GLOBALS['SourceDog'] = []
 *     }
 *
 * PHP will implicitly create the arrays in this case, without E_NOTICE.
 */
$GLOBALS['SourceDog']['Modules'][] = Modules\JsFileLinter::class;
$GLOBALS['SourceDog']['Modules'][] = Modules\PhpFileLinter::class;
$GLOBALS['SourceDog']['Modules'][] = Modules\CssFileLinter::class;
$GLOBALS['SourceDog']['Modules'][] = Modules\JsonFileLinter::class;
$GLOBALS['SourceDog']['Modules'][] = Modules\YamlFileLinter::class;
$GLOBALS['SourceDog']['Modules'][] = Modules\FileTreeMonitor::class;
$GLOBALS['SourceDog']['Modules'][] = Modules\TailMonitor::class;
$GLOBALS['SourceDog']['Modules'][] = Modules\PathFilter::class;
/**/
/*
    Old style, for reference:

    SourceFile::$linters[] = Linters\PhpFileLinter::class;
    SourceFile::$linters[] = Linters\CssFileLinter::class;
    SourceFile::$linters[] = Linters\JsonFileLinter::class;
    SourceFile::$linters[] = Linters\JsFileLinter::class;
    SourceFile::$linters[] = Linters\YamlFileLinter::class;
*/
