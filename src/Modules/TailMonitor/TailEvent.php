<?php
namespace SourceDog\Modules\TailMonitor;

use SourceDog\Cli;
use SourceDog\Events\FileEvent;

class TailEvent extends FileEvent {

    public function output() {
        Cli::info($this->data['message'], $this->data['file']);
    }

}

