<?php
namespace SourceDog\Modules\FileTreeMonitor;

use SourceDog\Cli;
use SourceDog\Event;

class InotifyWaitError extends Event {

    public function __construct(string $str) {
        parent::__construct(static::class, ['stderr' => $str]);
    }

    public function output() {
        Cli::debug('inotifywait: '.$this->data['stderr']);
    }
}
