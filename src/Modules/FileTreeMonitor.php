<?php
namespace SourceDog\Modules;

use SourceDog\Module;
use SourceDog\MonitorInterface;
use SourceDog\Process;
use SplFileInfo;
use SourceDog\Event;
use SourceDog\Events\FileChanged;
use SourceDog\Events\FileCreated;
use SourceDog\Events\FileDeleted;
use SourceDog\Cli;

/**
 * Uses inotifywait to recursively monitor a directory for file modifications
 */
class FileTreeMonitor extends Module implements MonitorInterface {

    protected $process;

    public static function getName() : string {
        return 'Source Tree monitor';
    }

    public function prepare_monitor() : void {
        if($this->process) return;
        $inotifywait = trim(shell_exec('which '.escapeshellarg('inotifywait')));
        if(!$inotifywait) {
               throw new \SourceDog\Exception("File tree monitoring requires inotifywait to exist in path");
        }
        $root = $this->sourceDog->getProjectRoot();
        $this->process = new Process($inotifywait.' -rm '.escapeshellarg($root));
        Cli::notice('Watching '.$root.' for file events');
    }

    public function getNextEvent() : ?Event {
        if(null !== ($str = $this->process->getStdOut())) {
            $str = trim($str);
            list($path, $events, $file) = explode(" ", $str.' -'); // The ' -' avoids notice for missing $file in $str when ISDIR
            $events = explode(",", $events);

            if(in_array('ISDIR', $events)) {
                $file = null;
            }

            // Modified file?
            if(in_array('CLOSE_WRITE', $events) && !in_array('IS_DIR', $events)) {
                return new FileChanged(new SplFileInfo($path.$file));
            }
            if(in_array('DELETE', $events)) {
                return new FileDeleted(new SplFileInfo($path.$file));
            }
            if(in_array('CREATE', $events)) {
                return new FileCreated(new SplFileInfo($path.$file));
            }

        }
        $this->process->getStdErr();
        return null;
    }
}
