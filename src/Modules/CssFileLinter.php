<?php
namespace SourceDog\Modules;

use F2\Cmd\Cmd;
use SourceDog\Module;
use SourceDog\CmdInterface;
use SourceDog\SourceFile;
use SourceDog\Report;
use SourceDog\LinterInterface;

class CssFileLinter extends Module implements LinterInterface, CmdInterface
{
    protected static $enabled = true;
    protected static $cssLinter;

    public static function getName() : string {
        return 'CSS file linter';
    }

    public static function getArguments() : array {
        return [
            '|no-css-lint' => 'Disable .css linting',
        ];
    }

    public static function parseArguments(Cmd $cmd) : void {
        if($cmd->flag('no-css-lint')) {
            static::$enabled = false;
        }
    }

    public function supports(SourceFile $file) : bool {
        if(!static::$enabled) return false;
        return $file->getPath()->getExtension() === 'css';
    }

    public function lint(SourceFile $file) : ?Report
    {
        $res = static::_lint($file->getPath()->getPathname());

        if(!$res) return null;

        // get line number from ""1: Unknown CSS property "borli" (line: 2, char: 10)""
        preg_match('|\(line:\ ([0-9]+),|', $res, $matches);

        $line = isset($matches[1]) ? intval($matches[1]) : null;

        return new Report($file, $res, $line, $this->sourceDog);
    }

    protected static function _lint($path)
    {
        if(!static::$cssLinter) {
            static::$cssLinter = new \CssLint\Linter();
        }

        if(static::$cssLinter->lintString(str_replace("\t", "    ", \SourceDog\Fs::file_get_contents($path)))) {
            return null;
        }
        $res = '';
    	$idx = 1;
    	foreach(static::$cssLinter->getErrors() as $error) {
    		$res .= $idx++.': '.$error."\n";
        }
        return trim($res);
    }
}
