<?php
namespace SourceDog\Modules;

use F2\Cmd\Cmd;
use SourceDog\CmdInterface;
use SourceDog\Module;
use SourceDog\SourceFile;
use SourceDog\Report;
use SourceDog\LinterInterface;
use SourceDog\Fs;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;

class YamlFileLinter extends Module implements LinterInterface, CmdInterface
{
    protected static $enabled = true;
    static $linter;

    public static function getName() : string {
        return 'YAML file linter';
    }

    public static function getArguments() : array {
        return [
            '|no-yaml-lint' => 'Disable .yaml linting',
        ];
    }

    public static function parseArguments(Cmd $cmd) : void {
        if($cmd->flag('no-yaml-lint')) {
            static::$enabled = false;
        }
    }

    public function supports(SourceFile $file) : bool {
        if(!static::$enabled) return false;
        return $file->getPath()->getExtension() === 'yaml';
    }

    public function lint(SourceFile $file) : ?Report
    {
        $content = Fs::file_get_contents($file->getPath());
        $parser = new Parser();
        try {
            $parser->parse($content, Yaml::PARSE_CONSTANT);
        } catch (ParseException $e) {
            $message = $e->getMessage();
            if(strpos($message, 'PARSE_CUSTOM_TAGS')!==false)
                $message = 'Invalid YAML';
            return new Report($file, $message, $e->getParsedLine(), $this->sourceDog);
        }
        return null;
    }
}
