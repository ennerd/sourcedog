<?php
namespace SourceDog\Modules;

use SourceDog\Cli;
use SourceDog\Process;
use SplFileInfo;
use SourceDog\Event;
use SourceDog\Modules\TailMonitor\TailEvent;
use SourceDog\Module;
use SourceDog\MonitorInterface;
use SourceDog\ConfigInterface;
use SourceDog\ConfigSpecification;

/**
 * Uses inotifywait to recursively monitor a directory for file modifications
 */
class TailMonitor extends Module implements MonitorInterface, ConfigInterface {

    protected $enabled = false;
    protected $tailCommand;
    protected $processes = [];
    protected $eventQueue = [];

    public static function getName() : string {
        return 'File append monitor';
    }

    public static function getConfigSpecifications() : array {
        return [
            new ConfigSpecification('tail', 'A list of file patterns to watch for new lines on', null, function($val) {
                if($val === null) return null;
                if(!is_array($val)) return 'Expects a list of patterns';
                foreach($val as $el) {
                    if(!is_string($el)) {
                        return 'Each element must be a string pattern';
                    }
                }
                return null;
            }, ['/var/log/apache2/*.log', '/var/log/nginx/*.log', '/var/log/php*.log', '/var/log/mysql/slow-queries.log']),
            ];
    }

    public function prepare_monitor() : void {
        if(empty($this->sourceDog->config->tail) || !is_array($this->sourceDog->config->tail)) {
            Cli::debug('No valid tail specified in sourcedog.yaml');
            return;
        }
        $this->tailCommand = trim(shell_exec('which '.escapeshellarg('tail')));
        if(!$this->tailCommand) {
               throw new \SourceDog\Exception("Tail monitoring requires tail to exist in path");
        }
        $this->enabled = true;

        foreach($this->sourceDog->config->tail as $pattern) {
            $files = glob($pattern);
            foreach($files as $file) {
                $this->processes[$file] = new Process($this->tailCommand.' -f -n 0 '.escapeshellarg($file));
                Cli::notice('Watching '.$file.' for new lines');
            }
        }
    }

    public function getNextEvent() : ?Event {
        if(!$this->enabled) {
            return null;
        }
        foreach($this->processes as $file => $process) {
            if(null !== ($str = $process->getStdOut())) {
                $this->eventQueue[] = new TailEvent(new SplFileInfo($file), trim($str));
            }
            if(null !== ($str = $process->getStdErr())) {
                Cli::warn('Unable to watch '.$file);
                unset($this->processes[$file]);
            }
        }
        if(sizeof($this->eventQueue) > 0) {
            return array_shift($this->eventQueue);
        }
        return null;
    }
}
