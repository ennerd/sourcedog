<?php
namespace SourceDog\Modules;

use F2\Cmd\Cmd;
use SourceDog\CmdInterface;
use SourceDog\Module;
use SourceDog\SourceFile;
use SourceDog\Report;
use SourceDog\LinterInterface;
use Seld\JsonLint\JsonParser;
use Seld\JsonLint\ParsingException;

class JsonFileLinter extends Module implements LinterInterface, CmdInterface
{
    protected static $enabled = true;
    static $linter;

    public static function getName() : string {
        return 'JSON file linter';
    }

    public static function getArguments() : array {
        return [
            '|no-json-lint' => 'Disable .json linting',
        ];
    }

    public static function parseArguments(Cmd $cmd) : void {
        if($cmd->flag('no-json-lint')) {
            static::$enabled = false;
        }
    }

    public function supports(SourceFile $file) : bool {
        if(!static::$enabled) return false;
        return $file->getPath()->getExtension() === 'json';
    }

    public function lint(SourceFile $file) : ?Report
    {
	    if(!($res = static::_lint($file->getPath()->getPathname()))) {
            return null;
        }

        // get line number from 'on line 1:'
        preg_match($regex = '|on line ([0-9]+)\:|', $res, $matches);

        $line = isset($matches[1]) ? intval($matches[1]) : null;

        return new Report($file, $res, $line, $this->sourceDog);
    }

    protected static function _lint($path)
    {
        if(!static::$linter) {
            static::$linter = new JsonParser();
        }

        $res = static::$linter->lint(\SourceDog\Fs::file_get_contents($path));
        if(!$res) return null;
        return $res->getMessage();
    }
}
