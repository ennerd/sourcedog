<?php
namespace SourceDog\Modules;

use SourceDog\Module;
use SourceDog\SourceFile;
use SourceDog\Report;
use SourceDog\LinterInterface;
use SourceDog\CmdInterface;
use F2\Cmd\Cmd;

class PhpFileLinter extends Module implements LinterInterface, CmdInterface
{
    protected static $enabled = true;

    public static function getName() : string {
        return 'PHP file linter';
    }

    public static function getArguments() : array {
        return [
            '|no-php-lint' => 'Disable PHP linting',
        ];
    }

    public static function parseArguments(Cmd $cmd) : void {
        if($cmd->flag('no-php-lint')) {
            static::$enabled = false;
        }
    }

    public function supports(SourceFile $file) : bool {
        if(!static::$enabled) return false;
        return $file->getPath()->getExtension() === 'php';
    }

    public function lint(SourceFile $file) : ?Report
    {
        if ($file->getPath()->getSize() > 1000000) {
            return new Report($file, "Gigantic PHP file", null, $this->sourceDog);
        }

        $res = static::_lint($file->getPath()->getPathname());
        if (strpos($res, 'No syntax errors') === 0) {
            return null;
        }

        // Identify line number
        $lineNumber = null;
        $found = preg_match('|on line (\d+)|', $res, $matches);
        if(isset($matches[1]) && is_numeric($matches[1])) {
            $lineNumber = intval($matches[1]);
        }

        return new Report($file, $res, $lineNumber, $this->sourceDog);
    }

    protected static function _lint($path)
    {
        static $php;
        if (!$php) {
            $php = trim(`/usr/bin/env which php`);
        }
        if (!$php) {
            throw new \SourceDog\Exception('Could not find php executable');
        }
        $res = trim(shell_exec($php.' -l '.escapeshellarg($path).' 2>&1'));
        return $res;
    }
}
