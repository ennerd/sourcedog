<?php
namespace SourceDog\Modules;

use SplFileInfo;
use SourceDog\Module;
use SourceDog\PathFilterInterface;
use SourceDog\ConfigInterface;
use SourceDog\ConfigSpecification;
use SourceDog\Cli;

class PathFilter extends Module implements PathFilterInterface, ConfigInterface {

    public static function getName() : string {
        return 'Path filter';
    }

    public static function getConfigSpecifications() : array {
        $invalidator = function($val) {
            if(!is_array($val)) return 'Expected an array';
            foreach($val as $el) {
                if(!is_string($el)) {
                    return 'Each value must be a string';
                }
            }
            return false;
        };

        return [
            new ConfigSpecification(
                'never_ignore',
                'A list of patterns relative to project root that will never be ignored',
                [ '.php$', '.js$', '.css$', '.json' ], 
                $invalidator),
            new ConfigSpecification(
                'ignore',
                'A list of patterns relative to project root that will be ignored', 
                [ '^/vendor/', '/.*', '*.tmp$', '*~$' ],
                $invalidator),
            ];
    }

    public function ignorePath(SplFileInfo $path) : ?bool {
        $path = substr($path, strlen($this->sourceDog->getProjectRoot()));
        if($this->sourceDog->config->never_ignore) {
            foreach($this->sourceDog->config->never_ignore as $pattern) {
                if(static::patternMatch($pattern, $path)) {
                    Cli::debug("Never ignoring '$path' due to pattern '$pattern'");
                    return false;
                }
            }
        }

        if($this->sourceDog->config->ignore) {
            foreach($this->sourceDog->config->ignore as $pattern) {
                if(static::patternMatch($pattern, $path)) {
                    Cli::debug("Ignoring '$path' due to pattern '$pattern'");
                    return true;
                }
            }
        }

        return null;
    }

    protected static function patternMatch($pattern, $path) {
            $pattern = str_replace(['\\.', '\\/', '\\*'], ['__FRODE_DOTT', '__FRODE_SLASH', '__FRODE_STJERNE'], $pattern);
            $pattern = str_replace(['|', '.', '/', '*'], ['\|', '\.', '\/', '[^\/]+'], $pattern);
            $pattern = str_replace(['__FRODE_DOTT', '__FRODE_SLASH', '__FRODE_STJERNE'], ['.', '/', '*'], $pattern);
            return preg_match("|$pattern|", $path, $matches) > 0;
    }
}
