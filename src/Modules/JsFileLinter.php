<?php
namespace SourceDog\Modules;

use F2\Cmd\Cmd;
use SourceDog\CmdInterface;
use SourceDog\Module;
use SourceDog\Report;
use SourceDog\LinterInterface;
use SourceDog\SourceFile;

class JsFileLinter extends Module implements LinterInterface, CmdInterface
{
    protected static $enabled = true;

    public static function getName() : string {
        return 'JS file linter';
    }

    public static function getArguments() : array {
        return [
            '|no-js-lint' => 'Disable .js linting',
        ];
    }

    public static function parseArguments(Cmd $cmd) : void {
        if($cmd->flag('no-js-lint')) {
            static::$enabled = false;
        }
    }

    public function supports(SourceFile $sourceFile) : bool {
        if(!static::$enabled) return false;
        return $sourceFile->getPath()->getExtension() === 'js';
    }

    public function lint(SourceFile $file) : ?Report
    {
        $res = static::_lint($file->getPath()->getPathname());
        if(!$res) return null;

        $path = $file->getPath()->getPathname();


        // get line number from ./sourcedog/tests/invalid.js:2
        preg_match($regex = '|^'.preg_quote($path).':([0-9]+)|', $res, $matches);

        $line = isset($matches[1]) ? intval($matches[1]) : null;

        return new Report($file, $res, null, $this->sourceDog);
    }

    protected static function _lint($path)
    {
        static $node;
        if (!$node) {
            $node = trim(`/usr/bin/env which node`);
        }
        if (!$node) {
            $node = trim(`/usr/bin/env which nodejs`);
        }
        if (!$node) {
            if(file_exists($node = __DIR__.'/../../../../bin/node'));
            else if(file_exists($node = __DIR__.'/../../vendor/bin/node'));
            else $node = null;
        }
        if (!$node) {
            throw new \SourceDog\Exception('Could not find node executable');
        }
        $res = trim(shell_exec($node.' --check '.escapeshellarg($path).' 2>&1'));
        return $res;
    }
}
