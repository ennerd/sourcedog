<?php
namespace SourceDog;

interface SourceFileInterface {
    public function supports(SourceFile $file) : bool;
}
