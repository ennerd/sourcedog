<?php
namespace SourceDog;

/**
 * Modules implementing this interface will be asked for their default configuration values
 * and a description of that configuration option
 */
interface ConfigInterface {

    /**
     * return an array of ConfigSpecification instances (array<ConfigSpecification>)
     */
    public static function getConfigSpecifications() : array;
}
