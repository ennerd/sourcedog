<?php
namespace SourceDog;

use SourceDog\Event;

/**
 * Modules that can emit events from various sources such as logs or file system.
 */
interface MonitorInterface {
    /**
     * Non-blocking check for new events.
     */
    public function getNextEvent() : ?Event;
}
