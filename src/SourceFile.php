<?php
namespace SourceDog;

class SourceFile
{
    protected $sourceDog;
    protected $path;
    public $checkCount = 0;

    /**
     * Callbacks that will receive an SplFileInfo instance that could be checked for problems.
     * Each callback must return null or a Report instance.
     */
    public static $processors = [];

    public function __construct(SourceDog $sourceDog, \SplFileInfo $path)
    {
        $this->sourceDog = $sourceDog;
        $this->path = $path;
    }

    public function getPath() : \SplFileInfo {
        return $this->path;
    }

    public function lint() : ?array
    {
        Cli::debug("SourceFile::lint(".$this->sourceDog->shortenPath($this->path->getPathname()).")");
        $reports = [];
        foreach ($this->sourceDog->getModuleInstances(LinterInterface::class) as $linter) {
            try {
                if(!$linter->supports($this))
                    continue;
                $this->checkCount++;
                $result = $linter->lint($this);
                if ($result !== null) {
                    $reports[] = $result;
                }
            } catch (\Throwable $e) {
                Cli::error('Checking "'.$this->sourceDog->shortenPath($this->path->getPathname()).'" caused '.get_class($e).'('.$e->getMessage().')');
            }
        }
        if (sizeof($reports)>0) {
            return $reports;
        }
        return null;
    }

    public function process() : ?Report {
        // Post processing is not yet implemented
        Cli::debug("SourceFile::process(".$this->sourceDog->shortenPath($this->path->getPathname()).") NOT IMPLEMENTED");
        return null;
    }
}
