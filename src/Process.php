<?php
namespace SourceDog;

class Process {

    protected $cmd;
    protected $proc;
    protected $pipes;

    public function __construct(string $cmd, string $cwd=NULL, array $env=NULL) {
        $this->cmd = $cmd;
        $this->proc = proc_open('exec '.$cmd, [
            ['pipe','r'],
            ['pipe', 'w'],
            ['pipe', 'w'],
        ], $this->pipes);
        if(!is_resource($this->proc)) {
            throw new Exception("Unable to start process '$cmd'");
        }

        fclose($this->pipes[0]);

        stream_set_blocking($this->pipes[1], 0);
        stream_set_blocking($this->pipes[2], 0);
    }

    public function __destruct() {
        Cli::debug('Terminating '.$this->cmd);
        fclose($this->pipes[1]);
        fclose($this->pipes[2]);
        proc_terminate($this->proc);
    }

    public function getStdOut() : ?string {
        $res = fgets($this->pipes[1]);
        if(!is_string($res))
            return null;
        return $res;
    }

    public function getStdErr() : ?string {
        $res = fgets($this->pipes[2]);
        if(!is_string($res)) {
            return null;
        }
        Cli::debug($this->cmd.': '.trim($res));
        return $res;
    }
}
