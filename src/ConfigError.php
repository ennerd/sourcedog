<?php
namespace SourceDog;

use Symfony\Component\Yaml\Yaml;

class ConfigError extends Exception {
    protected $sourceDog;
    protected $spec;

    public function __construct(ConfigSpecification $spec, $errorDescription) {
        $this->spec = $spec;
        parent::__construct($errorDescription);
    }

    public function getReport(SourceDog $sourceDog, \SplFileInfo $path) : Report {
        $message .= "Invalid '".$this->spec->getFieldName()."': ".$this->getMessage()."\n";
        $message .= "Example of a valid configuration:\n\n";
        $tempDef = [ $this->spec->getFieldName() => $this->spec->getExample() ];
        $message .= Yaml::dump($tempDef);

        return new Report(new SourceFile($sourceDog, $path), $message);
    }
}
