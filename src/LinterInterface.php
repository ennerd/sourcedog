<?php
namespace SourceDog;

use SplFileInfo;

interface LinterInterface extends SourceFileInterface {
    public function lint(SourceFile $file) : ?Report;
}
