<?php
namespace SourceDog;

interface Output {
    public function output();
}
