<?php
namespace SourceDog;
// From ennerd/fubber-cmd packagist

class Cmd {
    protected $optIndex;
    protected $args;
    protected $usage;
    public $argv;

    /**
    * $cmd = new Cmd('Description', [
    *   'h|help' => 'Show help',
    *   ]
    */
    public function __construct($usage, array $args=[], $mandatoryParams = []) {
        global $argv;
        $this->usage = $usage;
        $this->args = $args;
        $this->args['h|help'] = 'Show this help';

        $short = '';
        $long = [];
        foreach($this->args as $args => $desc) {
            $p = explode("|", $args);
            if(trim($p[0])!=='') {
                $short .= $p[0];
            }
            for($i = 1; $i < sizeof($p); $i++) {
                $long[] = $p[$i];
            }
        }
        $offset = 0;
        $opts = getopt($short, $long, $offset);

        for($i = 1; $i < $offset; $i++) {
            if(substr($argv[$i], 0, 2)=='--') {
// Check that this long argument is acceptable
                $tmp = substr($argv[$i], 2);
                if(!in_array($tmp, $long)) {
                    $this->error("unrecognized option '--$tmp'");
                    die();

                }
            } else if($argv[$i][0]=='-') {
// Check that there are no unknown arguments present
                $tmp = substr($argv[$i], 1);
                for($ii = 0; $ii < strlen($short); $ii++) {
                    $tmp = str_replace($short[$ii], '', $tmp);
                }
                if($tmp !== '') {
                    $this->error("invalid option -- '".$tmp[0]."'");
                    die();
                }
            }
        }

/*
        $offsetShouldBe = 1 + sizeof($opts);
        if($offsetShouldBe != $offset) {
            $this->error('unrecognized option \''.$argv[$offsetShouldBe].'\'');
var_dump($argv);
var_dump($opts);die();
            die(1);
        }

*/
        $this->argv = [$argv[0]];
        for($i = $offset; $i < sizeof($argv); $i++) {
            $this->argv[] = $argv[$i];
        }
//var_dump($this->argv);die();


        if($this->flag('h', 'help')) {
            $this->usage();
            die();
        }

        if(sizeof($mandatoryParams) > sizeof($this->argv)-1) {
            $this->error('missing parameter \''.$mandatoryParams[sizeof($this->argv)-1].'\'');
            die(2);
        }
    }

    /**
    * Checks if argument list contains the flag. For example:
    * if($cmd->flag('h', 'help')) {
    *   // show help
    * }
    */
    public function flag($short=null, ... $long) {
        return sizeof(getopt($short, $long)) > 0;
    }

    public function error($message) {
        global $argv;
        fwrite(STDERR, basename($argv[0]).': '.$message."\n");
        fwrite(STDERR, 'Try \''.basename($argv[0]).' --help\' for more information.'."\n");
    }

    public function usage() {
        global $argv;
        echo "Usage: ".basename($argv[0])."\n";
        echo trim($this->usage)."\n";
        if(sizeof($this->args)>0) {
            $lines = [];
            foreach($this->args as $k => $desc) {
                $parts = explode("|", $k);
                $opts = [];
                // short opts
                for($i = 0; $i < strlen($parts[0]); $i++) {
                    $char = $parts[0][$i];
                    if($char != ':')
                        $opts[] = '-'.$char;
                }
                for($i = 1; $i < sizeof($parts); $i++) {
                    if(($trimmed = rtrim($parts[$i], ':')) != $parts[$i]) {
                        $opts[] = '--'.$trimmed.'=VAL';
                    } else {
                        $opts[] = '--'.$parts[$i];
                    }
                }
                $lines[] = [implode(", ", $opts), $desc];
            }
            $maxWidth = 0;
            foreach($lines as $line) {
                if(strlen($line[0])>$maxWidth)
                    $maxWidth = strlen($line[0]);
            }
            echo "\n";
            foreach($lines as $line) {
                echo str_pad($line[0], $maxWidth)."\t".$line[1]."\n";
            }
        }
    }
}

