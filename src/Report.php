<?php
namespace SourceDog;

use SourceDog\Cli;
use SourceDog\Fs;
use SourceDog\Output;

class Report implements Output {

    protected $sourceDog;
    protected $file, $message, $lineNumber;
    protected $maxLines = 15;

    /**
     * Report something about a file in the source tree. $lineNumber cause highlighting of the line
     */
    public function __construct(SourceFile $file, string $message, int $lineNumber=null, SourceDog $sourceDog=null)
    {
        $this->sourceDog = $sourceDog;
        $this->file = $file;
        $this->message = $message;
        $this->lineNumber = $lineNumber;
    }

    public function output(): string
    {
        $message = $this->preprocess($this->message);

        $lines = explode("\n", trim($message));
        $tty = Cli::isTTY();
        if($tty) {
            $width = Cli::getTTYSize()[0] - 20;
        } else {
            $width = 160;
        }
        $hLine = str_repeat('─', $width);
        $result = "";
        if($tty) {
            $result .= "      ┌$hLine"."┐\n";
        }
        for ($lineNumber = 0; $lineNumber < $this->maxLines && $lineNumber < sizeof($lines); $lineNumber++) {
            $line = $lines[$lineNumber];
            if (strlen($line) > $width - 2) {
                $line = substr($line, 0, $width - 7) . ' [...]';
            }
            if($tty) {
                $result .= '      │ '.$this->highlightError($line).str_repeat(" ", $width - strlen($line) - 1)."│\n";
            } else {
                $result .= " $line\n";
            }
        }
        if ($lineNumber === $this->maxLines) {
            $line = '--- report truncated ---';
            if($tty) {
                $result .= '      │ '.Cli::csiWarn().str_repeat(" ", $width - strlen($line) - 1).Cli::csiReset()."│\n";
            } else {
                $result .= " $line\n";
            }
        }

/*
        $firstLine = 0;
        $firstCut = false;
        $lastLine = sizeof($lines)-1;
        $lastCut = false;

        if ($this->lineNumber !== null) {
            $firstLine = floor($this->lineNumber - $this->maxLines / 2);
            $lastLine = $firstLine + $this->maxLines;

            if ($firstLine < 0) {
                $lastLine += -$firstLine;
                $firstLine = 0;
            } else {
                $firstCut = true;
            }
            if ($lastLine - $firstLine > $this->maxLines) {
                $lastLine -= ($lastLine - $firstLine) - $this->maxLines;
                $lastCut = true;
            }
            if ($lastLine > sizeof($lines)) {
                $lastLine = sizeof($lines) - 1;
                $lastCut = false;
            }
        } else {
            if ($lastLine > $this->maxLines) {
                $lastLine = $this->maxLines - 1;
                $lastCut = true;
            }
        }

        if ($firstCut) {
            $line = "--- clipped beginning (firstLine=$firstLine lastLine=$lastLine lineNumber=".$this->lineNumber." maxLines=".$this->maxLines." total=".sizeof($lines).")---";
            if($tty) {
                $result .= '      │ '.Cli::csiWarn().$line.Cli::csiReset().str_repeat(" ", $width - strlen($line) - 1)."│\n";
            } else {
                $result .= " $line\n";
            }
        }
        if ($lastCut) {
            $line = "--- clipped end ($lastLine) ---";
            if($tty) {
                $result .= '      │ '.Cli::csiWarn().$line.Cli::csiReset().str_repeat(" ", $width - strlen($line) - 1)."│\n";
            } else {
                $result .= " $line\n";
            }
        }
*/
        if($this->lineNumber !== null) {
            // We're attaching a report
            if($tty) {
                $result .= "      ├$hLine"."┤\n";
            } else {
                $result .= " --\n";
            }
        } else {
            if($tty) {
                $result .= "      └$hLine"."┘\n";
            }
        }

//                           ---------------------------------------------------\n";
        if($this->lineNumber) {
            if(filesize($this->file->getPath()) < 10000000) {
                $fileLines = file($this->file->getPath());
                foreach($fileLines as $k => $line) {
                    $fileLines[$k] = rtrim($line);
                }
                $firstLine = max($this->lineNumber - 3, 0);
                $lastLine = min($this->lineNumber + 3, sizeof($fileLines));
                for($i = $firstLine; $i < $lastLine; $i++) {
                    $line = $fileLines[$i];
                    if (strlen($line) > $width - 2) {
                        $line = substr($line, 0, $width - 7) . ' [...]';
                    }

                    if($tty) {
echo "|$line|\n";
                        $result .=
                            str_pad($i+1, 5, " ", STR_PAD_LEFT).
                            " ".
                            ($i+1===$this->lineNumber ? Cli::csiHighlight().'█' : '│').
                            ' '.
                            $this->highlightCode($line).Cli::csiReset().str_repeat(" ", $width - strlen($line) - 1)."│\n";
                    } else {
                        $result .= ($i+1===$this->lineNumber ? ' >> ' : '    ').$line."\n";
                    }
                }
            }
            if($tty) {
                $result .= "      └$hLine"."┘\n";
            }
        }
        return $result;
//        \SourceDog\Cli::stdlog($result);
    }

    protected function preprocess(string $message): string {
        if ($this->sourceDog) {
            $projectRoot = $this->sourceDog->getProjectRoot()->getPath();
        }
        $message = str_replace($projectRoot.'/', './', $message);

        return $message;
    }

    protected function highlightCode($line) {
        return $line;
//        $regex = '/(([\.\+\-*\/&"\',{[()\]};:=?%|\\]+)|([a-zA-Z_\x80-\xff\$][a-zA-Z0-9_\x80-\xff]*))/';
echo "REGEX: $regex\n";
        $regex = '';
        $line = preg_replace_callback($regex, function($matches) {
            if (trim($matches[1], "'+-.,;:\"'\\/%&()[]{}=?")==='') {
                // token
                return Cli::csi("37;1m").$matches[1].Cli::csiReset();
            }
            echo "- matched ".$matches[1]."\n";
            return $matches[1];
            var_dump($matches);
            die();
        }, $line);
        return $line;
    }

    protected function highlightError($line)
    {
        //		$line = str_replace($this->file, Cli::csiWarn().$this->file.Cli::csiReset(), $line);

        $line = preg_replace_callback('|\/[\.a-zA-Z0-9_\-\/\*\?]+|', function ($matches) {
            return Cli::csiHighlight().$matches[0].Cli::csiReset();
            if (Fs::file_exists($matches[0])) {
                return Cli::csiWarn().$matches[0].Cli::csiReset();
            } else {
                return Cli::csiError().$matches[0].Cli::csiReset();
            }
        }, $line);

        $line = preg_replace_callback('|line \d+|', function ($matches) {
            return Cli::csiWarn().$matches[0].Cli::csiReset();
        }, $line);
        return $line;
    }
}
