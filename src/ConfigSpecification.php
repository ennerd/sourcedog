<?php
namespace SourceDog;

final class ConfigSpecification {
    protected $fieldName, $description, $defaultValue, $isInvalidChecker, $example;

    public function __construct(string $fieldName, string $description, $defaultValue=null, callable $isInvalidChecker=null, $example=null) {
        $this->fieldName = $fieldName;
        $this->description = $description;
        $this->defaultValue = $defaultValue;
        $this->isInvalidChecker = $isInvalidChecker;
        if($example === null && $defaultValue)
            $this->example = $defaultValue;
        else
            $this->example = $example;
    }

    public function getFieldName() : string {
        return $this->fieldName;
    }

    public function getDescription() : string {
        return $this->description;
    }

    public function getDefault() {
        return $this->defaultValue;
    }

    public function getExample() {
        return $this->example;
    }

    public function isInvalid($value) : ?string {
        if($this->isInvalidChecker) {
            $message = call_user_func($this->isInvalidChecker, $value);
            if($message) return $message;
        }
        return null;
    }

    public function applyTo(Config $config) : void {
        $fieldName = $this->fieldName;
        if(!property_exists($config, $fieldName) || $config->$fieldName === NULL) {
            $config->$fieldName = $this->defaultValue;
        }
        if($errorMessage = $this->isInvalid($config->$fieldName)) {
            throw new ConfigError($this, $errorMessage);
        }
    }
}
