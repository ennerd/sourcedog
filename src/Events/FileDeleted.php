<?php
namespace SourceDog\Events;

use SourceDog\Cli;

class FileDeleted extends FileEvent {

    public function __construct(\SplFileInfo $file) {
        parent::__construct($file, $file.' was deleted');
    }

}
