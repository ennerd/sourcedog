<?php
namespace SourceDog\Events;

use SourceDog\Cli;

class FileChanged extends FileEvent {

    public function __construct(\SplFileInfo $file) {
        parent::__construct($file, $file.' was modified');
    }

}

