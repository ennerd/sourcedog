<?php
namespace SourceDog\Events;

use SourceDog\Output;
use SourceDog\Cli;
use SourceDog\Event;

class FileEvent extends Event {

    public function __construct(\SplFileInfo $file, string $message) {
        parent::__construct(static::class, [ 'file' => $file, 'message' => $message ]);
    }

    public function output() {
        Cli::info($this->data['file'], static::class);
    }

}

