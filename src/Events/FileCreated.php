<?php
namespace SourceDog\Events;

use SourceDog\Cli;

class FileCreated extends FileEvent {

    public function __construct(\SplFileInfo $file) {
        parent::__construct($file, $file.' was created');
    }

}

