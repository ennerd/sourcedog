<?php
namespace SourceDog;

use F2\Cmd\Cmd;

/**
 * Modules that extend the options of the cmd interface
 */
interface CmdInterface {

    /**
     * Return an array of the form:
     *
     * [ 'x|example' => 'Example argument' ]
     */
    public static function getArguments() : array;

    /**
     * Parse the arguments:
     *
     * $flagWasSet = $cmd->flag('x', 'example') : bool
     */
    public static function parseArguments(Cmd $cmd) : void;
}

