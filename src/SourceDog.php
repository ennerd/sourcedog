<?php
namespace SourceDog;

use F2\Cmd\Cmd;

class SourceDog
{
    public $config;
    protected $modules;
    protected $projectRoot;
    protected $cache = [];

    public static function cli()
    {
        $description = 'SourceDog monitors and lints your source code. By creating a sourcedog.yaml file in
your project root, it can also watch the contents of log files and database tables
for new rows.

You can create an options file \'sourcedog.yaml\' in your project root to configure more features. Find 
\'sourcedog.yaml-example\' for documentation.';

        // Options related to the SourceDog core
        $options = [
            's|scan' =>         'Scan source tree before monitoring',
            'm|monitor' =>      'Keep running and monitor for changed files (skips full scan unless -s or --scan)',
            'd|debug' =>        'Output debug log',
            'b|bell' =>         'Disable bell sound on errors while monitoring',
        ];

        // CmdInterface modules can add arguments to the specification
        foreach(static::getModuleClasses(CmdInterface::class) as $module) {
            $options += $module::getArguments();
        }

        $cmd = new Cmd($description, $options);

        $config = new Config();
        if($cmd->flag('debug')) {
            Config::$debug = true;
        }
        if($cmd->flag('bell')) {
            Config::$bell = false;
        }
        if($cmd->flag('monitor')) {
            $config->monitor = true;
        }
        if($cmd->flag('scan') || !$cmd->flag('monitor')) {
            $config->scan = true;
        }

        foreach(static::getModuleClasses(CmdInterface::class) as $module) {
            Cli::debug($module.' parsing arguments');
            $module::parseArguments($cmd);
        }

        try {
            // Find project root

            if (isset($cmd->argv[1]) && is_dir($cmd->argv[1])) {
                $projectRoot = realpath($cmd->argv[1]);
            } else {
                $projectRoot = $_SERVER['PWD'];
                while(!file_exists($projectRoot.'/sourcedog.yaml')) {
                    $newProjectRoot = dirname($projectRoot);
                    if($newProjectRoot === $projectRoot) {
                        // We have reached the top. Use current dir instead.
                        $projectRoot = $_SERVER['PWD'];
                        break;
                    }
                    $projectRoot = $newProjectRoot;
                }
            }
            $instance = new static(new \SplFileInfo($projectRoot), $config);
        } catch (Exception $e) {
            Cli::error($e->getMessage());
        }
        echo Cli::csiEraseInLine();
    }

    public function __construct(\SplFileInfo $projectRoot, Config $config = null)
    {
        Cli::info("SourceDog starting...");
        Cli::debug("Project root=$projectRoot");
        if($config === null) $config = new Config();
        $this->projectRoot = new \SplFileInfo($projectRoot);
        $this->config = $config;
        if(file_exists($configFile = $this->projectRoot."/sourcedog.yaml")) {
            $this->config->parseOptionsFile(new \SplFileInfo($configFile));
        } else {
            $configFile = null;
        }

        $configErrors = [];
        foreach(static::getModuleClasses(ConfigInterface::class) as $module) {
            foreach($module::getConfigSpecifications() as $spec) {
                try {
                    $spec->applyTo($this->config);
                } catch (ConfigError $e) {
                    if(!$configFile) {
                        throw new Exception("Configuration error on field '".$spec->getFieldName()."', but no configuration error: ".$e->getMessage());
                    } else {
                        $configErrors[] = $e->getReport($this, new \SplFileInfo($configFile));
                    }
                }
            }
        }
        if(sizeof($configErrors)>0) {
            Cli::error("Errors parsing '".$this->shortenPath($configFile)."':");
            if(Cli::isTTY()) {
                Cli::stderr("      ".Cli::csiHighlight()."sourcedog.yaml:".Cli::csiReset()."\n");
            }
            foreach($configErrors as $report) {
                Cli::stderr($report->output());
            }
            exit(1);
        }

        $hadErrors = false;
        $this->modules = [];
        foreach($GLOBALS['SourceDog']['Modules'] as $module) {
            if(!class_exists($module)) {
                Cli::error("- module $module could not be found");
                $hadErrors = true;
            } else {
                Cli::debug("- module $module added");
            }
            $this->modules[] = new $module($this);
        }
        if($hadErrors) {
                Cli::error("ABORTING");
                return;
        }

        if($this->config->scan) {
            Cli::notice('Beginning source tree scan');
            foreach($this->modules as $module) {
                $module->do_prepare_scan();
            }
            $this->scan();
        }

        if($this->config->monitor) {
            Cli::notice('Beginning source monitoring');
            foreach($this->modules as $module) {
                $module->do_prepare_monitor();
            }
            $this->monitor();
        }
    }

    public function __destruct()
    {
        Cli::debug("SourceDog terminating");
    }

    /**
     * Check file for syntax problems. Return null if no check was made.
     * Return true if everything is ok, and false if not ok.
     */
    public function lintFile(\SplFileInfo $path) : ?bool {
        $source = new SourceFile($this, $path);
        $reports = $source->lint();
        if($reports !== null) {
            Cli::error('Linting '.$this->shortenPath($path).' error');
            if(Cli::isTTY()) {
//                Cli::stderr("      ".Cli::csiHighlight().substr($path, strlen($this->projectRoot) + 1).":".Cli::csiReset()."\n");
            }
            foreach ($reports as $report) {
                Cli::stderr($report->output());
            }
            return false;
        } else if($source->checkCount === 0) {
            return null;
        } else {
            return true;
        }
    }

    /**
     * Invoked whenever a file is monitored or whenever a full scan
     * is performed. For each file.
     */
    public function processFile(\SplFileInfo $path) : ?Report {
        $source = new SourceFile($this, $path);
        return $source->process();
    }

    /**
     * Run PathFilterInterface modules on the provided path
     */
    public function isPathIgnored(\SplFileInfo $path) : ?bool {
        foreach($this->getModuleInstances(PathFilterInterface::class) as $module) {
            if(null !== ($res = $module->ignorePath($path))) {
                return $res;
            }
        }
        return null;
    }

    /**
     * Launches various monitors. 
     */
    public function monitor() {
        Cli::info("Waiting for monitor events");
        $fastUntil = microtime(true);

        while(true) {
            foreach($this->getModuleInstances(MonitorInterface::class) as $monitor) {
                if($event = $monitor->getNextEvent()) {
                    if(is_a($event, Events\FileEvent::class)) {
                        if($this->isPathIgnored($event->data['file'])) {
                            continue;
                        }
                    }
                    if(is_a($event, Events\FileChanged::class)) {
                        $res = $this->lintFile($event->data['file']);
                        if($res === true) {
                            Cli::success($event->data['file'].' linted OK!');
                            $this->processFile($event->data['file']);
                        } else {
                            Cli::bell();
                        }
                    } else {
                        $event->output();
                    }
                    // We won't usleep between every file - because many changes can occur in builks
                    $fastUntil = microtime(true) + 0.1;
                }
            }

            if($fastUntil <= microtime(true)) {
                usleep(50000);
            }
        }
    }

    public function scan(\SplFileInfo $root=null)
    {
        if ($root === null) {
            $root = $this->projectRoot;
        }
        Cli::info("Starting recursive scan from ".$this->shortenPath($root));

        $queue[] = new \FilesystemIterator($root, \FilesystemIterator::CURRENT_AS_FILEINFO | \FilesystemIterator::SKIP_DOTS);
        while ($iterator = array_shift($queue)) {
            if( Cli::isTTY() ) {
                echo Cli::iterator()." ".$this->shortenPath($iterator->getPathname()).Cli::csiEraseInLine()."\r";
            }
            foreach ($iterator as $path) {
                if($this->isPathIgnored($path)) {
                    continue;
                } elseif ($path->isDir()) {
                    $queue[] = new \FilesystemIterator($path, \FilesystemIterator::CURRENT_AS_FILEINFO | \FilesystemIterator::SKIP_DOTS);
                } elseif ($path->isFile()) {
                    if( Cli::isTTY() ) {
                        echo Cli::iterator()." ".$this->shortenPath($path).Cli::csiEraseInLine()."\r";
                    }
                    $this->lintFile($path);
                    $this->processFile($path);
                } else {
                    Cli::debug("Unknown file type at ".$this->shortenPath($path).". Ignoring.");
                }
            }
        }
        Cli::csiEraseInLine();
    }

    public function shortenPath($path) {
        if (\is_a($path, \SplFileInfo::class)) {
            return $this->shortenPath($path->getPathname());
        }

        if (rtrim($this->projectRoot, '/') === rtrim($path, '/')) {
            return './';
        }

        if (($pos = strpos($path.'', $this->projectRoot.'')) === 0) {
            return './'.substr($path, strlen($this->projectRoot) + 1).(is_dir($path) ? '/' : '');
        }
        return $path;
    }

    public static function phpCsFixer() {
        static $result;
        if ($result) {
            return $result;
        }
        // Check if we downloaded a version
        $uniqueName = get_current_user().'phpcsfixer';

        if (file_exists($path = sys_get_temp_dir().'/'.$uniqueName) && fileowner($path)==getmyuid() && is_executable($path)) {
            return $path;
        }
        $envPath = trim(shell_exec('/usr/bin/env which php-cs-fixer'));
        if ($envPath && is_executable($envPath)) {
            return $result = $envPath;
        }
        // Fallback
        $phar = file_get_contents('https://cs.symfony.com/download/php-cs-fixer-v2.phar');
        file_put_contents($path, '');
        $mask = umask(0);
        chmod($path, 0700);
        if (file_get_contents($path)==='') {
            $fp = fopen($path, 'wb');
            fwrite($fp, $phar);
            fclose($fp);
        }
        return $result = '/usr/local/php '.$path;
    }

    public function getProjectRoot() : \SplFileInfo {
        return $this->projectRoot;
    }

    public function getModuleInstances($interfaceName) {
        foreach($this->modules as $module) {
            if($module instanceof $interfaceName) {
                yield $module;
            }
        }
    }

    public static function getModuleClasses($interfaceName) {
        foreach($GLOBALS['SourceDog']['Modules'] as $className) {
            if(in_array($interfaceName, class_implements($className))) {
                yield $className;
            }
        }
    }
}
