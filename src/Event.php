<?php
namespace SourceDog;

class Event implements Output {

    public $type;
    public $data;

    public function __construct(string $type, array $data=null) {
        $this->type = $type;
        $this->data = $data;
    }

    public function output() {
        Cli::stdlog("EVENT ".$this->type);
    }
}
