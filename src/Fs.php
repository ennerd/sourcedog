<?php
namespace SourceDog;

class Fs
{
    public static function file_exists($path)
    {
        return file_exists($path);
    }

    public static function file_get_contents($path)
    {
        return file_get_contents($path);
    }
}
