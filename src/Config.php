<?php
namespace SourceDog;

use Symfony\Component\Yaml\Yaml;

class Config {

    public function parseOptionsFile(\SplFileInfo $path) {
        Cli::notice("Using config file $path");
        $options = Yaml::parseFile($path);

        foreach($options as $option => $value) {
            if($option==='debug' || $option==='bell' || !property_exists(static::class, $option) || $this->$option === null) {
                if($option === 'debug') {
                    static::$debug = !!$value;
                } elseif($option === 'bell') {
                    static::$bell = !!$value;
                } else {
                    $this->$option = $value;
                }
                if($value === true) $value = 'true';
                elseif($value === false) $value = 'false';
                elseif(is_string($value)) $value = "'$value'";
                elseif(is_array($value)) $value = '[array]';
                Cli::debug(basename($path).": option $option=$value");
            } else {
                Cli::debug(basename($path).": option=$option was ignored");
            }
        }

/*
        if(isset($options['tail'])) {
            foreach($options['tail'] as $path) {
                foreach(glob($path) as $p) {
                    $this->tail[] = new \SplFileInfo($p);
                }
            }
        }
        if(isset($options['ignore'])) {
            // Setting ignore in sourcedog.yaml overwrites defaults
            $this->ignore = [];
            foreach($options['ignore'] as $path) {
                $this->ignore[] = $path;
            }
        }
*/

    }

    // Static config variable to define if we emit debug logging
    public static $debug = null;

    // Bell sound on errors while monitoring (disable with -b or --bell)
    public static $bell = null;

    // Monitor files
    public $monitor = null;

    // Perform a full source scan
    public $scan = null;
}
