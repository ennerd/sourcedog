<?php
namespace SourceDog;

abstract class Module {
    protected $sourceDog;

    /**
     * Modules are initialized by SourceDog, so this constructor can't be overridden
     */
    final public function __construct(SourceDog $sourceDog) {
        $this->sourceDog = $sourceDog;
    }

    /**
     * Return the module name which should be used throughout
     */
    abstract public static function getName() : string;

    /**
     * Override this function in your module if you need to perform any early
     * phase initialization. If your module only does monitoring, override the
     * prepare_scan method instead.
     */
    protected function initialize() : void {}

    /**
     * Override this function in your module if you need to prepare for monitoring
     */
    protected function prepare_monitor() : void {}

    /**
     * Override this function in your module if you need to prepare for scanning
     */
    protected function prepare_scan() : void {}

    /**
     * *** Internal code for managing modules below ***
     */

    // Variables used to prevent double initialization and preparations
    private $_done_initialize = false;
    private $_done_prepare_scan = false;
    private $_done_prepare_monitor = false;

    /**
     * Perform the initialization procedure only once
     */
    final public function do_initialize() : void {
        if($this->_done_initialize) {
            return;
        }
        $this->_done_initialize = true;
        if($this->is_overridden('initialize')) {
            Cli::debug($this->getName().': initializing');
            $this->initialize();
            Cli::debug($this->getName().': initialized');
        }
    }

    /**
     * Perform the prepare scan procedure only once
     */
    final public function do_prepare_scan() : void {
        if($this->_done_prepare_scan) {
            return;
        }
        $this->_done_prepare_scan = true;
        if($this->is_overridden('prepare_scan')) {
            Cli::debug($this->getName().': preparing scan');
            $this->prepare_scan();
            Cli::debug($this->getName().': scan prepared');
        } else {
            Cli::debug($this->getName().': no scan preparation needed');
        }
    }

    /**
     * Perform the prepare monitor procedure only once
     */
    final public function do_prepare_monitor() : void {
        if($this->_done_prepare_monitor) {
            return;
        }
        $this->_done_prepare_monitor = true;
        if($this->is_overridden('prepare_monitor')) {
            Cli::debug($this->getName().': preparing monitoring');
            $this->prepare_monitor();
            Cli::debug($this->getName().': monitoring prepared');
        } else {
            Cli::debug($this->getName().': no monitor preparation needed');
        }
    }

    /**
     * Method to check if the module has implemented the named method
     */
    protected function is_overridden($methodName) : bool {
        $reflector = new \ReflectionMethod($this, $methodName);
        return $reflector->getDeclaringClass()->getName() !== self::class;
    }
}
