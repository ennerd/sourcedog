<?php
namespace SourceDog;

use SplFileInfo;

interface PathFilterInterface  {
    /**
     * Returns null, true or false to filter the provided path:
     *
     * null     The path matched no filters. Continue processing.
     * false    The path must be processed. Stop further filtering by other modules.
     * true     The path must not be processed. Stop further filtering by other modules.
     */
    public function ignorePath(SplFileInfo $path) : ?bool;
}

